package com.gds.userservice.enums;

public enum Role {
    ADMIN, USER
}
