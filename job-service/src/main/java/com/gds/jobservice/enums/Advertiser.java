package com.gds.jobservice.enums;

public enum Advertiser {
    EMPLOYEE, CUSTOMER
}
