package com.gds.jobservice.enums;

public enum AdvertStatus {
    OPEN, CLOSED, CANCELLED, ASSIGNED, REVIEWED
}
