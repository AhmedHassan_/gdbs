package com.gds.jobservice.enums;

public enum OfferStatus {
    OPEN, CLOSED, ACCEPTED, REJECTED
}
