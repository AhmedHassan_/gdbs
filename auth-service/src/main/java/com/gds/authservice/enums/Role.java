package com.gds.authservice.enums;

public enum Role {
    ADMIN, USER
}
